'use strict';

module.exports = exports = require('./browser');

exports.Page = require('./page');
exports.Test = require('./test');
