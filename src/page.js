'use strict';

var Test = require('./test')
  , error = require('./util/error');

var Page = module.exports = exports = function (browser) {
  this.browser = browser;
};

// Add test namespace

Object.defineProperty(Page.prototype, 'test', {
  get: function () {
    return new Test(this);
  }
});

Page.prototype.enqueue = function (fn) {
  var page = this;
  page.browser.enqueue(fn);
  return page;
};

Page.prototype.eval = function (clientFn /*, [params...[, cb]] */) {
  var page = this
    , browser = page.browser;
  var _arguments = arguments;
  return page.enqueue(function (done) {
    var args = []
      , cb = null;
    for (var i = 1; i < _arguments.length; i++) {
      var arg = _arguments[i];
      if (typeof arg == 'function') {
        cb = arg;
        break;
      } else {
        args.push(arg);
      }
    }
    console.log('eval(<fn>, %s)', args);
    browser._page.evaluate.apply(browser._page, [
      clientFn, function (err, result) {
        if (err) return done(err);
        if (cb) cb(result);
        done();
      }
    ].concat(args));
  });
};

Page.prototype.wait = function (param) {
  var page = this;
  if (typeof param == 'number')
    return page.delay(param);
  if (typeof param == 'string')
    return page.waitFor(param);
  if (typeof param == 'function')
    return page.waitFn(param);
  throw error('wait accepts milliseconds, selector or a function');
};

Page.prototype.delay = function (delay) {
  var page = this;
  return page.enqueue(function (done) {
    console.log('delay %s ms', delay);
    setTimeout(done, delay);
  });
};

Page.prototype.waitFn = function (fn) {
  var page = this
    , browser = page.browser
    , start = Date.now();
  function check(done) {
    browser._page.evaluate(fn, function (err, result) {
      if (err) return done(err);
      if (result) return done();
      if (Date.now() > (start + browser.options.timeout))
        return done(error('waitFn timed out'));
      setTimeout(function () {
        check(done);
      }, browser.options.pollInterval);
    });
  }
  return page.enqueue(function (done) {
    console.log('waitFn(<fn>)');
    check(done);
  });
};

Page.prototype.waitFor = function (sel) {
  var page = this
    , browser = page.browser
    , start = Date.now()
    , selector = sel;
  function check(done) {
    browser._page.evaluate(function (selector) {
        return document.querySelectorAll(selector).length;
      }, selector
      , function (err, result) {
        if (err) return done(err);
        if (result) return done();
        if (Date.now() > (start + browser.options.timeout))
          return done(error('waitFor(%s) timed out', selector));
        setTimeout(function () {
          check(done);
        }, browser.options.pollInterval);
      });
  }
  return page.enqueue(function (done) {
    console.log('waitFor(%s)', selector);
    check(done);
  });
};

Page.prototype.val = function (sel, value) {
  var page = this
    , browser = page.browser
    , selector = sel;
  return page.enqueue(function (done) {
    console.log('val(%s, %s)', selector, value);
    browser._page.evaluate(function (selector, value) {
        var element = document.querySelector(selector);
        if (!element)
          return false;
        if (typeof element.value == 'undefined')
          return false;
        element.value = value;
        var ev = document.createEvent('HTMLEvents');
        ev.initEvent('change', false, true);
        element.dispatchEvent(ev);
        return true;
      }, selector
      , value
      , function (err, found) {
        if (err) return done(err);
        if (!found)
          return done(error('val: %s not found / not an input', selector));
        return done();
      });
  });
};

Page.prototype.click = function (sel) {
  var page = this
    , browser = page.browser
    , selector = sel;
  return page.enqueue(function (done) {
    console.log('click(%s)', selector);
    browser._page.evaluate(function (selector) {
        var element = document.querySelector(selector);
        if (!element)
          return false;
        var ev = document.createEvent('MouseEvents');
        ev.initEvent('click', true, true);
        element.dispatchEvent(ev);
        return true;
      }, selector
      , function (err, found) {
        if (err) return done(err);
        if (!found)
          return done(error('val: %s not found', selector));
        done();
      });
  });
};


